const config = require('config');
const mongojs = require('mongojs');

module.exports = class Query{
    constructor(){
        this.db = mongojs(config.get('mongo_host'));
        this.farm = this.db.collection('Farm');
    }

    getAllFarm(){
        return new Promise((resolve, reject) => {
            this.farm.find((err, result) => {
                if(err) reject(err.toString());
                if(result.length > 0) result.forEach( obj => obj.id = obj._id)
                resolve(result);  
            });
        });
    }

    getFarm(ctx){
        return new Promise((resolve, reject) => {
            this.farm.findOne({_id:  mongojs.ObjectId(mongojs.ObjectId(ctx.id))}, (err, result) => {
                if(err) reject(err.toString());
                if(result.hasOwnProperty('_id')) result.id = result._id
                resolve(result);  
            });
        });
    }

    addFarm(ctx){
        return new Promise((resolve, reject) => {
            let insert_data = {};
            let props = ['name', 'owner_id', 'employee_ids'];
            for (let i in props){
                if(ctx.hasOwnProperty(props[i])) insert_data[props[i]] = ctx[props[i]];
            }
            this.farm.insert(insert_data, (err, result) => {
                if(err) reject(err.toString());
                 resolve({
                    message: 'New farm created!',
                    data: result
                });
            })
        });
    }

    updateFarm(ctx){
        return new Promise((resolve, reject) => {
            this.farm.findOne({_id: mongojs.ObjectId(mongojs.ObjectId(ctx.id))}, (err, doc) => {
                if(err) reject(err.toString());
                if(doc === undefined){
                    resolve({message: 'odject not found!'});
                } else {
                    let updated_data = {};
                    let props = ['name', 'owner_id', 'employee_ids'];
                    for (let i in props){
                        if(ctx.hasOwnProperty(props[i])) updated_data[props[i]] = ctx[props[i]];
                    }
                    this.farm.update({_id:  mongojs.ObjectId(mongojs.ObjectId(ctx.id))}, {$set: updated_data}, {multi: false}, (err, result) => {
                        if (err) reject(err.toString());
                        resolve({
                            message: 'Update farm!',
                            data: result
                        })   
                    })
                }
            });         
        });
    }

    deleteFarm(ctx){
        return new Promise((resolve, reject) => {
            this.farm.remove({_id: mongojs.ObjectId(mongojs.ObjectId(ctx.id))}, (err, result) => {
                if(err) reject(err.toString());
                resolve({
                    status: "success",
                    message: "Farm deleted"
                });
            });
        });
    }

    resetFarm(){
        return new Promise((resolve, reject) => {
            this.farm.drop((err, result) => {
                if(err) reject(err.toString());
                resolve(true);
            });
        });
    }
}