const config = require('config');

module.exports = class Farm {
    constructor() {
        let Query;
        switch(config.get('db_type')){
            case('mongo'):
                Query = require('./database/mongo');
                break;
        }
        
        this.query = new Query();
    }

    getAllFarm(){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.getAllFarm();
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    getFarm(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.getFarm(ctx);
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    addFarm(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.addFarm(ctx);
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    updateFarm(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.updateFarm(ctx);
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    deleteFarm(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.deleteFarm(ctx);
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    resetFarm(){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.resetFarm();
                resolve('success');
            } catch (err) {
                reject(err.toString());
            }
        });
    }
    
}