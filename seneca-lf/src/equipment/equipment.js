const config = require('config');

module.exports = class Equipment {
    constructor() {
        let Query;
        switch(config.get('db_type')){
            case('mongo'):
                Query = require('./database/mongo');
                break;
        }
        
        this.query = new Query();
    }

    getAllEquipment(){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.getAllEquipment();
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    getEquipment(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.getEquipment(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    addEquipment(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.addEquipment(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    updateEquipment(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.updateEquipment(ctx);
                resolve(result);
            } catch (err) {
                resolve(err);
            }
        });
    }

    deleteEquipment(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.deleteEquipment(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    resetEquipment(){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.resetEquipment();
                resolve('success');
            } catch (err) {
                resolve(err.toString());
            }
        });
    }
    
}