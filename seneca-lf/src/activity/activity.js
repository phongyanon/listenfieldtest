const config = require('config');

module.exports = class Activity {
    constructor() {
        let Query;
        switch(config.get('db_type')){
            case('mongo'):
                Query = require('./database/mongo');
                break;
        }
        
        this.query = new Query();
    }

    getAllActivity(){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.getAllActivity();
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    getActivity(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.getActivity(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    addActivity(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.addActivity(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    updateActivity(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.updateActivity(ctx);
                resolve(result);
            } catch (err) {
                resolve(err);
            }
        });
    }

    deleteActivity(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.deleteActivity(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    resetActivity(){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.resetActivity();
                resolve('success');
            } catch (err) {
                resolve(err.toString());
            }
        });
    }
    
}