const request = require('request');
const config = require('config');

const Farm = require('./farm/farm');
let farm = new Farm();

const Employee = require('./employee/employee');
let employee = new Employee();

const PlantPlot = require('./plantplot/plantplot');
let plantplot = new PlantPlot();

const Activity = require('./activity/activity');
let activity = new Activity();

const Process = require('./process/process');
let process = new Process();

const Equipment = require('./equipment/equipment');
let equipment = new Equipment();

const Plant = require('./plant/plant');
let plant = new Plant();

module.exports = function(options = {}) {

    this.add('ms:farm,cmd:add', async (msg, respond) => {
        try {
            let result = await farm.addFarm(msg.data);
            respond(null, {result: result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:farm,cmd:getAll', async (msg, respond) => {
        try {
            let result = await farm.getAllFarm();
            respond(null, {result: result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:farm,cmd:get', async (msg, respond) => {
        try {
            let result = await farm.getFarm(msg.data);
            respond(null, {result: result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:farm,cmd:update', async (msg, respond) => {
        try {
            let result = await farm.updateFarm(msg.data);
            respond(null, {result: result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:farm,cmd:delete', async (msg, respond) => {
        try {
            let result = await farm.deleteFarm(msg.data);
            respond(null, {result: result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:employee,cmd:add', async (msg, respond) => {
        try {
            let result = await employee.addEmployee(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:employee,cmd:getAll', async (msg, respond) => {
        try {
            let result = await employee.getAllEmployee();
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:employee,cmd:get', async (msg, respond) => {
        try {
            let result = await employee.getEmployee(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:employee,cmd:update', async (msg, respond) => {
        try {
            let result = await employee.updateEmployee(msg.data);
            respond(null, {result});          
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:employee,cmd:delete', async (msg, respond) => {
        try {
            let result = await employee.deleteEmployee(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:plantplot,cmd:add', async (msg, respond) => {
        try {
            let result = await plantplot.addPlantPlot(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:plantplot,cmd:getAll', async (msg, respond) => {
        try {
            let result = await plantplot.getAllPlantPlot();
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:plantplot,cmd:get', async (msg, respond) => {
        try {
            let result = await plantplot.getPlantPlot(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:plantplot,cmd:update', async (msg, respond) => {
        try {
            let result = await plantplot.updatePlantPlot(msg.data);
            respond(null, {result});          
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:plantplot,cmd:delete', async (msg, respond) => {
        try {
            let result = await plantplot.deletePlantPlot(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:activity,cmd:add', async (msg, respond) => {
        try {
            let result = await activity.addActivity(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:activity,cmd:getAll', async (msg, respond) => {
        try {
            let result = await activity.getAllActivity();
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:activity,cmd:get', async (msg, respond) => {
        try {
            let result = await activity.getActivity(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:activity,cmd:update', async (msg, respond) => {
        try {
            let result = await activity.updateActivity(msg.data);
            respond(null, {result});          
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:activity,cmd:delete', async (msg, respond) => {
        try {
            let result = await activity.deleteActivity(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:process,cmd:add', async (msg, respond) => {
        try {
            let result = await process.addProcess(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:process,cmd:getAll', async (msg, respond) => {
        try {
            let result = await process.getAllProcess();
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:process,cmd:get', async (msg, respond) => {
        try {
            let result = await process.getProcess(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:process,cmd:update', async (msg, respond) => {
        try {
            let result = await process.updateProcess(msg.data);
            respond(null, {result});          
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:process,cmd:delete', async (msg, respond) => {
        try {
            let result = await process.deleteProcess(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:equipment,cmd:add', async (msg, respond) => {
        try {
            let result = await equipment.addEquipment(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:equipment,cmd:getAll', async (msg, respond) => {
        try {
            let result = await equipment.getAllEquipment();
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:equipment,cmd:get', async (msg, respond) => {
        try {
            let result = await equipment.getEquipment(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:equipment,cmd:update', async (msg, respond) => {
        try {
            let result = await equipment.updateEquipment(msg.data);
            respond(null, {result});          
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:equipment,cmd:delete', async (msg, respond) => {
        try {
            let result = await equipment.deleteEquipment(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:plant,cmd:add', async (msg, respond) => {
        try {
            let result = await plant.addPlant(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:plant,cmd:getAll', async (msg, respond) => {
        try {
            let result = await plant.getAllPlant();
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:plant,cmd:get', async (msg, respond) => {
        try {
            let result = await plant.getPlant(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:plant,cmd:update', async (msg, respond) => {
        try {
            let result = await plant.updatePlant(msg.data);
            respond(null, {result});          
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:plant,cmd:delete', async (msg, respond) => {
        try {
            let result = await plant.deletePlant(msg.data);
            respond(null, {result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.wrap('ms:*', function(msg, respond){ // this must be the last function.
        // check token expired time and verify token
        let that = this;
        if (config.get('env_name') === 'production'){
            let options = {
                url: config.get('authen_url'),
                headers: {
                    'Authorization': `Bearer ${msg.accessToken}`
                }
            }
            request(options, function(err, result){
                if(err) respond(401, {error: err});
                result = JSON.parse(result.body);
                if (result.status === 'success') that.prior(msg, respond);
                else respond(401, result.body);
            });
        } else {
            that.prior(msg, respond);
        }
    });
}
