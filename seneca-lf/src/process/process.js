const config = require('config');

module.exports = class Process {
    constructor() {
        let Query;
        switch(config.get('db_type')){
            case('mongo'):
                Query = require('./database/mongo');
                break;
        }
        
        this.query = new Query();
    }

    getAllProcess(){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.getAllProcess();
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    getProcess(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.getProcess(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    addProcess(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.addProcess(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    updateProcess(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.updateProcess(ctx);
                resolve(result);
            } catch (err) {
                resolve(err);
            }
        });
    }

    deleteProcess(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.deleteProcess(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    resetProcess(){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.resetProcess();
                resolve('success');
            } catch (err) {
                resolve(err.toString());
            }
        });
    }
    
}