const config = require('config');

module.exports = class Plant {
    constructor() {
        let Query;
        switch(config.get('db_type')){
            case('mongo'):
                Query = require('./database/mongo');
                break;
        }
        
        this.query = new Query();
    }

    getAllPlant(){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.getAllPlant();
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    getPlant(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.getPlant(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    addPlant(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.addPlant(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    updatePlant(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.updatePlant(ctx);
                resolve(result);
            } catch (err) {
                resolve(err);
            }
        });
    }

    deletePlant(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.deletePlant(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    resetPlant(){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.resetPlant();
                resolve('success');
            } catch (err) {
                resolve(err.toString());
            }
        });
    }
    
}