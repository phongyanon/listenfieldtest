const config = require('config');
const mongojs = require('mongojs');

module.exports = class Query{
    constructor(){
        this.db = mongojs(config.get('mongo_host'));
        this.collection = this.db.collection('Plant');
    }

    getAllPlant(){
        return new Promise((resolve, reject) => {
            this.collection.find((err, result) => {
                if(err) {
                    resolve({
                            success: false,
                            message: err.toString(),
                            data: null
                        });
                }
                if(result.length > 0) result.forEach( obj => obj.id = obj._id)
                resolve({
                        success: true,
                        message: 'Get all plant',
                        data: result
                    });
            });
        });
    }

    getPlant(ctx){
        return new Promise((resolve, reject) => {
            this.collection.findOne({_id: mongojs.ObjectId(ctx.id)}, (err, result) => {
                if(err){
                    resolve({
                        success: false,
                        message: err.toString(),
                        data: null
                    });
                }
                if(result.hasOwnProperty('_id')) result.id = result._id
                resolve(result);  
            });
        });
    }

    addPlant(ctx){
        return new Promise((resolve, reject) => {
            let insert_data = {};
            let props = ['name', 'description'];
            for (let i in props){
                if(ctx.hasOwnProperty(props[i])) insert_data[props[i]] = ctx[props[i]];
            }
            this.collection.insert(insert_data, (err, result) => {
                if(err){
                    resolve({
                        success: false,
                        message: err.toString(),
                        data: null
                    });
                }
                 resolve({
                    message: 'New plant created!',
                    data: result
                });
            })
        });
    }

    updatePlant(ctx){
        return new Promise((resolve, reject) => {
            this.collection.findOne({_id: mongojs.ObjectId(ctx.id)}, (err, doc) => {
                if(err){
                    resolve({
                        success: false,
                        message: err.toString(),
                        data: null
                    });
                }
                if(doc === undefined){
                    resolve({message: 'odject not found!'});
                } else {
                    let updated_data = {};
                    let props = ['name', 'description'];
                    for (let i in props){
                        if(ctx.hasOwnProperty(props[i])) updated_data[props[i]] = ctx[props[i]];
                    }
                    this.collection.update({_id:  mongojs.ObjectId(ctx.id)}, {$set: updated_data}, {multi: false}, (err, result) => {
                        if (err){
                            resolve({
                                success: false,
                                message: err.toString(),
                                data: null
                            });
                        }
                        resolve({
                            message: 'Update plant!',
                            data: result
                        })   
                    })
                }
            });         
        });
    }

    deletePlant(ctx){
        return new Promise((resolve, reject) => {
            this.collection.remove({_id: mongojs.ObjectId(ctx.id)}, (err, result) => {
                if(err){
                    resolve({
                        success: false,
                        message: err.toString(),
                        data: null
                    });
                }
                resolve({
                    status: "success",
                    message: "Plant deleted"
                });
            });
        });
    }

    resetPlant(){
        return new Promise((resolve, reject) => {
            this.collection.drop((err, result) => {
                if(err) reject(err.toString());
                resolve(true);
            });
        });
    }
}