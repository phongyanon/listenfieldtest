const config = require('config');

module.exports = class PlantPlot {
    constructor() {
        let Query;
        switch(config.get('db_type')){
            case('mongo'):
                Query = require('./database/mongo');
                break;
        }
        
        this.query = new Query();
    }

    getAllPlantPlot(){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.getAllPlantPlot();
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    getPlantPlot(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.getPlantPlot(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    addPlantPlot(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.addPlantPlot(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    updatePlantPlot(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.updatePlantPlot(ctx);
                resolve(result);
            } catch (err) {
                resolve(err);
            }
        });
    }

    deletePlantPlot(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.deletePlantPlot(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    resetPlantPlot(){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.resetPlantPlot();
                resolve('success');
            } catch (err) {
                resolve(err.toString());
            }
        });
    }
    
}