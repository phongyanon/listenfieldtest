const config = require('config');

module.exports = class Employee {
    constructor() {
        let Query;
        switch(config.get('db_type')){
            case('mongo'):
                Query = require('./database/mongo');
                break;
        }
        
        this.query = new Query();
    }

    getAllEmployee(){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.getAllEmployee();
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    getEmployee(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.getEmployee(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    addEmployee(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.addEmployee(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    updateEmployee(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.updateEmployee(ctx);
                resolve(result);
            } catch (err) {
                resolve(err);
            }
        });
    }

    deleteEmployee(ctx){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.deleteEmployee(ctx);
                resolve(result);
            } catch (err) {
                resolve(err.toString());
            }
        });
    }

    resetEmployee(){
        return new Promise( async (resolve) => {
            let query = this.query;
            try {
                let result = await query.resetEmployee();
                resolve('success');
            } catch (err) {
                resolve(err.toString());
            }
        });
    }
    
}