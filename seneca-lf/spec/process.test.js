const config = require('config');
const Seneca = require('seneca');
const Process = require('../src/process/process');
const utils = require('./helpers/utils');
const db_type = config.get('db_type');

let process = new Process();

function test_seneca (done) {
    return Seneca({log: 'test'})
  
    // activate unit test mode. Errors provide additional stack tracing context.
    // The done callback is called when an error occurs anywhere.
      .test(done)
  
    // Load the microservice business logic
      .use('../src/services')
}

let mockdata = {
        name: "name1",
    process_type: "process_type1",
    description: "description1",
    income: "income1",
    expense: "expense1",
    equipment_ids: "equipment_ids1",
    employee_ids: "employee_ids1",
    start_time: "start_time1",
    end_time: "end_time1",
    status: "status1"
}

let mockdata2 = {
        name: "name2",
    process_type: "process_type2",
    description: "description2",
    income: "income2",
    expense: "expense2",
    equipment_ids: "equipment_ids2",
    employee_ids: "employee_ids2",
    start_time: "start_time2",
    end_time: "end_time2",
    status: "status2"
}

let mockdata3 = {
        name: "name3",
    process_type: "process_type3",
    description: "description3",
    income: "income3",
    expense: "expense3",
    equipment_ids: "equipment_ids3",
    employee_ids: "employee_ids3",
    start_time: "start_time3",
    end_time: "end_time3",
    status: "status3"
}


describe('Process Microservice Testing', function(){
    let seneca;
    before(function(done){
        seneca = test_seneca(done);
        process.resetProcess().then((res) => {
            console.log('Truncate Process Database');
            done();
        }).catch(err => console(err));
    });

    it('create new process', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'process', cmd: 'add', data: mockdata}, function(ignore, result){
            expect(result.result.message).to.equal('New process created!');
            done();
        });
    });

    it('get new process', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'process', cmd: 'getAll'}, function(ignore, result){
            let result_process = result.result.data[0];

            // console.log('get new process: ', result_process);

                        expect(result_process).to.have.property("name");
            expect(result_process).to.have.property("process_type");
            expect(result_process).to.have.property("description");
            expect(result_process).to.have.property("income");
            expect(result_process).to.have.property("expense");
            expect(result_process).to.have.property("equipment_ids");
            expect(result_process).to.have.property("employee_ids");
            expect(result_process).to.have.property("start_time");
            expect(result_process).to.have.property("end_time");
            expect(result_process).to.have.property("status");


            let t_id = utils.getIdByDB(db_type, result_process);
            mockdata2.id = t_id;
            done();
        });
    });

    it('update process', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'process', cmd: 'update', data: mockdata2}, function(ignore, result){
            // console.log('update process: ', result);
            expect(result.result.message).to.equal('Update process!');
            done();
        });
    });

    it('update not found process', function(done){
        let seneca = test_seneca(done);
        let mock_not_found = {
            id: 99,
            name: "name_not_found",
            process_type: "process_type_not_found",
            description: "description_not_found",
            income: "income_not_found",
            expense: "expense_not_found",
            equipment_ids: "equipment_ids_not_found",
            employee_ids: "employee_ids_not_found",
            start_time: "start_time_not_found",
            end_time: "end_time_not_found",
            status: "status_not_found"
        };
        seneca.act({ms: 'process', cmd: 'update', data: mock_not_found}, function(ignore, result){
            let result_process = result.result;

            expect(result_process.success).to.equal(false);
            expect(result_process.data).to.equal(null);

            done();
        });
    });

    it('get updated process', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'process', cmd: 'get', data: {id: mockdata2.id}}, function(ignore, result){
            // console.log('get update process: ', result);
            let result_process = result.result.data;

                        expect(result_process).to.have.property("name");
            expect(result_process).to.have.property("process_type");
            expect(result_process).to.have.property("description");
            expect(result_process).to.have.property("income");
            expect(result_process).to.have.property("expense");
            expect(result_process).to.have.property("equipment_ids");
            expect(result_process).to.have.property("employee_ids");
            expect(result_process).to.have.property("start_time");
            expect(result_process).to.have.property("end_time");
            expect(result_process).to.have.property("status");


            done();
        });
    });

    it('create new process again', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'process', cmd: 'add', data: mockdata3}, function(ignore, result){
            expect(result.result.message).to.equal('New process created!');
            done();
        });
    });

    it('get all process', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'process', cmd: 'getAll'}, function(ignore, result){
            // console.log('get all process: ', result.result);

            let results = result.result.data;
            expect(results.length).to.equal(2);
            
            for(let i=0; i<results.length; i++){
                            expect(results[i]).to.have.property("name");
            expect(results[i]).to.have.property("process_type");
            expect(results[i]).to.have.property("description");
            expect(results[i]).to.have.property("income");
            expect(results[i]).to.have.property("expense");
            expect(results[i]).to.have.property("equipment_ids");
            expect(results[i]).to.have.property("employee_ids");
            expect(results[i]).to.have.property("start_time");
            expect(results[i]).to.have.property("end_time");
            expect(results[i]).to.have.property("status");

            }

            done();
        });
    });

    it('delete process', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'process', cmd: 'delete', data: {id: mockdata2.id}}, function(ignore, result){
            // console.log('delete process: ', result);
            let result_process = result.result;

            expect(result_process.success).to.equal(true);
            expect(result_process.message).to.equal('Process deleted');

            done();
        });
    });

    it('get all process to make sure deleted process', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'process', cmd: 'getAll'}, function(ignore, result){
            let results = result.result.data;
            expect(results.length).to.equal(1);

            done();
        });
    });
});