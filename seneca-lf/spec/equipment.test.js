const config = require('config');
const Seneca = require('seneca');
const Equipment = require('../src/equipment/equipment');
const utils = require('./helpers/utils');
const db_type = config.get('db_type');

let equipment = new Equipment();

function test_seneca (done) {
    return Seneca({log: 'test'})
  
    // activate unit test mode. Errors provide additional stack tracing context.
    // The done callback is called when an error occurs anywhere.
      .test(done)
  
    // Load the microservice business logic
      .use('../src/services')
}

let mockdata = {
        name: "name1",
    description: "description1"
}

let mockdata2 = {
        name: "name2",
    description: "description2"
}

let mockdata3 = {
        name: "name3",
    description: "description3"
}


describe('Equipment Microservice Testing', function(){
    let seneca;
    before(function(done){
        seneca = test_seneca(done);
        equipment.resetEquipment().then((res) => {
            console.log('Truncate Equipment Database');
            done();
        }).catch(err => console(err));
    });

    it('create new equipment', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'equipment', cmd: 'add', data: mockdata}, function(ignore, result){
            expect(result.result.message).to.equal('New equipment created!');
            done();
        });
    });

    it('get new equipment', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'equipment', cmd: 'getAll'}, function(ignore, result){
            let result_equipment = result.result.data[0];

            // console.log('get new equipment: ', result_equipment);

                        expect(result_equipment).to.have.property("name");
            expect(result_equipment).to.have.property("description");


            let t_id = utils.getIdByDB(db_type, result_equipment);
            mockdata2.id = t_id;
            done();
        });
    });

    it('update equipment', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'equipment', cmd: 'update', data: mockdata2}, function(ignore, result){
            // console.log('update equipment: ', result);
            expect(result.result.message).to.equal('Update equipment!');
            done();
        });
    });

    it('update not found equipment', function(done){
        let seneca = test_seneca(done);
        let mock_not_found = {
            id: 99,
            name: "name_not_found",
            description: "description_not_found"
        };
        seneca.act({ms: 'equipment', cmd: 'update', data: mock_not_found}, function(ignore, result){
            let result_equipment = result.result;

            expect(result_equipment.success).to.equal(false);
            expect(result_equipment.data).to.equal(null);

            done();
        });
    });

    it('get updated equipment', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'equipment', cmd: 'get', data: {id: mockdata2.id}}, function(ignore, result){
            // console.log('get update equipment: ', result);
            let result_equipment = result.result.data;

                        expect(result_equipment).to.have.property("name");
            expect(result_equipment).to.have.property("description");


            done();
        });
    });

    it('create new equipment again', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'equipment', cmd: 'add', data: mockdata3}, function(ignore, result){
            expect(result.result.message).to.equal('New equipment created!');
            done();
        });
    });

    it('get all equipment', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'equipment', cmd: 'getAll'}, function(ignore, result){
            // console.log('get all equipment: ', result.result);

            let results = result.result.data;
            expect(results.length).to.equal(2);
            
            for(let i=0; i<results.length; i++){
                            expect(results[i]).to.have.property("name");
            expect(results[i]).to.have.property("description");

            }

            done();
        });
    });

    it('delete equipment', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'equipment', cmd: 'delete', data: {id: mockdata2.id}}, function(ignore, result){
            // console.log('delete equipment: ', result);
            let result_equipment = result.result;

            expect(result_equipment.success).to.equal(true);
            expect(result_equipment.message).to.equal('Equipment deleted');

            done();
        });
    });

    it('get all equipment to make sure deleted equipment', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'equipment', cmd: 'getAll'}, function(ignore, result){
            let results = result.result.data;
            expect(results.length).to.equal(1);

            done();
        });
    });
});