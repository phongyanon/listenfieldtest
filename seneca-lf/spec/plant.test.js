const config = require('config');
const Seneca = require('seneca');
const Plant = require('../src/plant/plant');
const utils = require('./helpers/utils');
const db_type = config.get('db_type');

let plant = new Plant();

function test_seneca (done) {
    return Seneca({log: 'test'})
  
    // activate unit test mode. Errors provide additional stack tracing context.
    // The done callback is called when an error occurs anywhere.
      .test(done)
  
    // Load the microservice business logic
      .use('../src/services')
}

let mockdata = {
        name: "name1",
    description: "description1"
}

let mockdata2 = {
        name: "name2",
    description: "description2"
}

let mockdata3 = {
        name: "name3",
    description: "description3"
}


describe('Plant Microservice Testing', function(){
    let seneca;
    before(function(done){
        seneca = test_seneca(done);
        plant.resetPlant().then((res) => {
            console.log('Truncate Plant Database');
            done();
        }).catch(err => console(err));
    });

    it('create new plant', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'plant', cmd: 'add', data: mockdata}, function(ignore, result){
            expect(result.result.message).to.equal('New plant created!');
            done();
        });
    });

    it('get new plant', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'plant', cmd: 'getAll'}, function(ignore, result){
            let result_plant = result.result.data[0];

            // console.log('get new plant: ', result_plant);

                        expect(result_plant).to.have.property("name");
            expect(result_plant).to.have.property("description");


            let t_id = utils.getIdByDB(db_type, result_plant);
            mockdata2.id = t_id;
            done();
        });
    });

    it('update plant', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'plant', cmd: 'update', data: mockdata2}, function(ignore, result){
            // console.log('update plant: ', result);
            expect(result.result.message).to.equal('Update plant!');
            done();
        });
    });

    it('update not found plant', function(done){
        let seneca = test_seneca(done);
        let mock_not_found = {
            id: 99,
            name: "name_not_found",
            description: "description_not_found"
        };
        seneca.act({ms: 'plant', cmd: 'update', data: mock_not_found}, function(ignore, result){
            let result_plant = result.result;

            expect(result_plant.success).to.equal(false);
            expect(result_plant.data).to.equal(null);

            done();
        });
    });

    it('get updated plant', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'plant', cmd: 'get', data: {id: mockdata2.id}}, function(ignore, result){
            // console.log('get update plant: ', result);
            let result_plant = result.result.data;

                        expect(result_plant).to.have.property("name");
            expect(result_plant).to.have.property("description");


            done();
        });
    });

    it('create new plant again', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'plant', cmd: 'add', data: mockdata3}, function(ignore, result){
            expect(result.result.message).to.equal('New plant created!');
            done();
        });
    });

    it('get all plant', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'plant', cmd: 'getAll'}, function(ignore, result){
            // console.log('get all plant: ', result.result);

            let results = result.result.data;
            expect(results.length).to.equal(2);
            
            for(let i=0; i<results.length; i++){
                            expect(results[i]).to.have.property("name");
            expect(results[i]).to.have.property("description");

            }

            done();
        });
    });

    it('delete plant', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'plant', cmd: 'delete', data: {id: mockdata2.id}}, function(ignore, result){
            // console.log('delete plant: ', result);
            let result_plant = result.result;

            expect(result_plant.success).to.equal(true);
            expect(result_plant.message).to.equal('Plant deleted');

            done();
        });
    });

    it('get all plant to make sure deleted plant', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'plant', cmd: 'getAll'}, function(ignore, result){
            let results = result.result.data;
            expect(results.length).to.equal(1);

            done();
        });
    });
});