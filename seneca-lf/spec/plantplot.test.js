const config = require('config');
const Seneca = require('seneca');
const PlantPlot = require('../src/plantplot/plantplot');
const utils = require('./helpers/utils');
const db_type = config.get('db_type');

let plantplot = new PlantPlot();

function test_seneca (done) {
    return Seneca({log: 'test'})
  
    // activate unit test mode. Errors provide additional stack tracing context.
    // The done callback is called when an error occurs anywhere.
      .test(done)
  
    // Load the microservice business logic
      .use('../src/services')
}

let mockdata = {
        name: "name1",
    farm_id: "farm_id1",
    activity_ids: "activity_ids1"
}

let mockdata2 = {
        name: "name2",
    farm_id: "farm_id2",
    activity_ids: "activity_ids2"
}

let mockdata3 = {
        name: "name3",
    farm_id: "farm_id3",
    activity_ids: "activity_ids3"
}


describe('PlantPlot Microservice Testing', function(){
    let seneca;
    before(function(done){
        seneca = test_seneca(done);
        plantplot.resetPlantPlot().then((res) => {
            console.log('Truncate PlantPlot Database');
            done();
        }).catch(err => console(err));
    });

    it('create new plantplot', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'plantplot', cmd: 'add', data: mockdata}, function(ignore, result){
            expect(result.result.message).to.equal('New plantplot created!');
            done();
        });
    });

    it('get new plantplot', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'plantplot', cmd: 'getAll'}, function(ignore, result){
            let result_plantplot = result.result.data[0];

            // console.log('get new plantplot: ', result_plantplot);

                        expect(result_plantplot).to.have.property("name");
            expect(result_plantplot).to.have.property("farm_id");
            expect(result_plantplot).to.have.property("activity_ids");


            let t_id = utils.getIdByDB(db_type, result_plantplot);
            mockdata2.id = t_id;
            done();
        });
    });

    it('update plantplot', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'plantplot', cmd: 'update', data: mockdata2}, function(ignore, result){
            // console.log('update plantplot: ', result);
            expect(result.result.message).to.equal('Update plantplot!');
            done();
        });
    });

    it('update not found plantplot', function(done){
        let seneca = test_seneca(done);
        let mock_not_found = {
            id: 99,
            name: "name_not_found",
            farm_id: "farm_id_not_found",
            activity_ids: "activity_ids_not_found"
        };
        seneca.act({ms: 'plantplot', cmd: 'update', data: mock_not_found}, function(ignore, result){
            let result_plantplot = result.result;

            expect(result_plantplot.success).to.equal(false);
            expect(result_plantplot.data).to.equal(null);

            done();
        });
    });

    it('get updated plantplot', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'plantplot', cmd: 'get', data: {id: mockdata2.id}}, function(ignore, result){
            // console.log('get update plantplot: ', result);
            let result_plantplot = result.result.data;

                        expect(result_plantplot).to.have.property("name");
            expect(result_plantplot).to.have.property("farm_id");
            expect(result_plantplot).to.have.property("activity_ids");


            done();
        });
    });

    it('create new plantplot again', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'plantplot', cmd: 'add', data: mockdata3}, function(ignore, result){
            expect(result.result.message).to.equal('New plantplot created!');
            done();
        });
    });

    it('get all plantplot', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'plantplot', cmd: 'getAll'}, function(ignore, result){
            // console.log('get all plantplot: ', result.result);

            let results = result.result.data;
            expect(results.length).to.equal(2);
            
            for(let i=0; i<results.length; i++){
                            expect(results[i]).to.have.property("name");
            expect(results[i]).to.have.property("farm_id");
            expect(results[i]).to.have.property("activity_ids");

            }

            done();
        });
    });

    it('delete plantplot', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'plantplot', cmd: 'delete', data: {id: mockdata2.id}}, function(ignore, result){
            // console.log('delete plantplot: ', result);
            let result_plantplot = result.result;

            expect(result_plantplot.success).to.equal(true);
            expect(result_plantplot.message).to.equal('PlantPlot deleted');

            done();
        });
    });

    it('get all plantplot to make sure deleted plantplot', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'plantplot', cmd: 'getAll'}, function(ignore, result){
            let results = result.result.data;
            expect(results.length).to.equal(1);

            done();
        });
    });
});