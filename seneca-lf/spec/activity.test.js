const config = require('config');
const Seneca = require('seneca');
const Activity = require('../src/activity/activity');
const utils = require('./helpers/utils');
const db_type = config.get('db_type');

let activity = new Activity();

function test_seneca (done) {
    return Seneca({log: 'test'})
  
    // activate unit test mode. Errors provide additional stack tracing context.
    // The done callback is called when an error occurs anywhere.
      .test(done)
  
    // Load the microservice business logic
      .use('../src/services')
}

let mockdata = {
        name: "name1",
    process_ids: "process_ids1",
    plant_id: "plant_id1",
    farm_id: "farm_id1"
}

let mockdata2 = {
        name: "name2",
    process_ids: "process_ids2",
    plant_id: "plant_id2",
    farm_id: "farm_id2"
}

let mockdata3 = {
        name: "name3",
    process_ids: "process_ids3",
    plant_id: "plant_id3",
    farm_id: "farm_id3"
}


describe('Activity Microservice Testing', function(){
    let seneca;
    before(function(done){
        seneca = test_seneca(done);
        activity.resetActivity().then((res) => {
            console.log('Truncate Activity Database');
            done();
        }).catch(err => console(err));
    });

    it('create new activity', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'activity', cmd: 'add', data: mockdata}, function(ignore, result){
            expect(result.result.message).to.equal('New activity created!');
            done();
        });
    });

    it('get new activity', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'activity', cmd: 'getAll'}, function(ignore, result){
            let result_activity = result.result.data[0];

            // console.log('get new activity: ', result_activity);

                        expect(result_activity).to.have.property("name");
            expect(result_activity).to.have.property("process_ids");
            expect(result_activity).to.have.property("plant_id");
            expect(result_activity).to.have.property("farm_id");


            let t_id = utils.getIdByDB(db_type, result_activity);
            mockdata2.id = t_id;
            done();
        });
    });

    it('update activity', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'activity', cmd: 'update', data: mockdata2}, function(ignore, result){
            // console.log('update activity: ', result);
            expect(result.result.message).to.equal('Update activity!');
            done();
        });
    });

    it('update not found activity', function(done){
        let seneca = test_seneca(done);
        let mock_not_found = {
            id: 99,
            name: "name_not_found",
            process_ids: "process_ids_not_found",
            plant_id: "plant_id_not_found",
            farm_id: "farm_id_not_found"
        };
        seneca.act({ms: 'activity', cmd: 'update', data: mock_not_found}, function(ignore, result){
            let result_activity = result.result;

            expect(result_activity.success).to.equal(false);
            expect(result_activity.data).to.equal(null);

            done();
        });
    });

    it('get updated activity', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'activity', cmd: 'get', data: {id: mockdata2.id}}, function(ignore, result){
            // console.log('get update activity: ', result);
            let result_activity = result.result.data;

                        expect(result_activity).to.have.property("name");
            expect(result_activity).to.have.property("process_ids");
            expect(result_activity).to.have.property("plant_id");
            expect(result_activity).to.have.property("farm_id");


            done();
        });
    });

    it('create new activity again', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'activity', cmd: 'add', data: mockdata3}, function(ignore, result){
            expect(result.result.message).to.equal('New activity created!');
            done();
        });
    });

    it('get all activity', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'activity', cmd: 'getAll'}, function(ignore, result){
            // console.log('get all activity: ', result.result);

            let results = result.result.data;
            expect(results.length).to.equal(2);
            
            for(let i=0; i<results.length; i++){
                            expect(results[i]).to.have.property("name");
            expect(results[i]).to.have.property("process_ids");
            expect(results[i]).to.have.property("plant_id");
            expect(results[i]).to.have.property("farm_id");

            }

            done();
        });
    });

    it('delete activity', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'activity', cmd: 'delete', data: {id: mockdata2.id}}, function(ignore, result){
            // console.log('delete activity: ', result);
            let result_activity = result.result;

            expect(result_activity.success).to.equal(true);
            expect(result_activity.message).to.equal('Activity deleted');

            done();
        });
    });

    it('get all activity to make sure deleted activity', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'activity', cmd: 'getAll'}, function(ignore, result){
            let results = result.result.data;
            expect(results.length).to.equal(1);

            done();
        });
    });
});