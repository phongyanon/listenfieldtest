const config = require('config');
const Seneca = require('seneca');
const Farm = require('../src/farm/farm');
const utils = require('./helpers/utils');
const db_type = config.get('db_type');

let farm = new Farm();

function test_seneca (done) {
    return Seneca({log: 'test'})
  
    // activate unit test mode. Errors provide additional stack tracing context.
    // The done callback is called when an error occurs anywhere.
      .test(done)
  
    // Load the microservice business logic
      .use('../src/services')
}

let mockdata = {
    name: 'My Farm',
    owner_id: 1,
    employee_ids: [1,2,3]
}

let mockdata2 = {
    name: 'Platform Farm',
    owner_id: 2,
    employee_ids: [4,5]
}

let mockdata3 = {
    name: 'LF Farm',
    owner_id: 3,
    employee_ids: [6]
}

describe('Farm Collection Testing', function(){
    let seneca;
    before(function(done){
        seneca = test_seneca(done);
        farm.resetFarm().then((res) => {
            console.log('Truncate Farm Collection');
            done();
        }).catch(err => console.log(err));
    });

    it('create new farm', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'farm', cmd: 'add', data: mockdata}, function(ignore, result){
            expect(result.result.message).to.equal('New farm created!');
            done();
        });
    });

    it('get new farm', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'farm', cmd: 'getAll'}, function(ignore, result){
            let result_farm = result.result[0];

            // console.log('get new farm: ', result_farm);

            expect(result_farm).to.have.property('name');
            expect(result_farm).to.have.property('owner_id');
            expect(result_farm).to.have.property('employee_ids');

            let t_id = utils.getIdByDB(db_type, result_farm);
            mockdata2.id = t_id;
            done();
        });
    });

    it('update farm', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'farm', cmd: 'update', data: mockdata2}, function(ignore, result){
            // console.log('update farm: ', result);
            expect(result.result.message).to.equal('Update farm!');
            done();
        });
    });

    // TODO
    // it('update not found farm', function(done){
    //     let seneca = test_seneca(done);
    //     seneca.act({ms: 'farm', act: 'update', data: mockdata2}, function(ignore, result){
    //         done();
    //     });
    // });

    it('get updated farm', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'farm', cmd: 'get', data: {id: mockdata2.id}}, function(ignore, result){
            // console.log('get updateed farm: ', result);
            let result_farm = result.result;

            expect(result_farm).to.have.property('name');
            expect(result_farm).to.have.property('owner_id');
            expect(result_farm).to.have.property('employee_ids');

            done();
        });
    });

    it('create new farm again', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'farm', cmd: 'add', data: mockdata3}, function(ignore, result){
            expect(result.result.message).to.equal('New farm created!');
            done();
        });
    });

    it('get all farm', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'farm', cmd: 'getAll'}, function(ignore, result){
            // console.log('get all farm: ', result);

            let results = result.result;
            expect(results.length).to.equal(2);
            
            for(let i=0; i<results.length; i++){
                expect(results[i]).to.have.property('name');
                expect(results[i]).to.have.property('owner_id');
                expect(results[i]).to.have.property('employee_ids');
            }

            done();
        });
    });

    it('delete farm', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'farm', cmd: 'delete', data: {id: mockdata2.id}}, function(ignore, result){
            // console.log('delete farm: ', result);
            let result_farm = result.result;

            expect(result_farm.status).to.equal('success');
            expect(result_farm.message).to.equal('Farm deleted');

            done();
        });
    });

    it('get all farm to make sure deleted farm', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'farm', cmd: 'getAll'}, function(ignore, result){
            let results = result.result;
            expect(results.length).to.equal(1);

            done();
        });
    });
});