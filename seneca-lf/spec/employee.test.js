const config = require('config');
const Seneca = require('seneca');
const Employee = require('../src/employee/employee');
const utils = require('./helpers/utils');
const db_type = config.get('db_type');

let employee = new Employee();

function test_seneca (done) {
    return Seneca({log: 'test'})
  
    // activate unit test mode. Errors provide additional stack tracing context.
    // The done callback is called when an error occurs anywhere.
      .test(done)
  
    // Load the microservice business logic
      .use('../src/services')
}

let mockdata = {
        firstname: "firstname1",
    lastname: "lastname1",
    farm_id: "farm_id1",
    description: "description1"
}

let mockdata2 = {
        firstname: "firstname2",
    lastname: "lastname2",
    farm_id: "farm_id2",
    description: "description2"
}

let mockdata3 = {
        firstname: "firstname3",
    lastname: "lastname3",
    farm_id: "farm_id3",
    description: "description3"
}


describe('Employee Microservice Testing', function(){
    let seneca;
    before(function(done){
        seneca = test_seneca(done);
        employee.resetEmployee().then((res) => {
            console.log('Truncate Employee Database');
            done();
        }).catch(err => console(err));
    });

    it('create new employee', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'employee', cmd: 'add', data: mockdata}, function(ignore, result){
            expect(result.result.message).to.equal('New employee created!');
            done();
        });
    });

    it('get new employee', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'employee', cmd: 'getAll'}, function(ignore, result){
            let result_employee = result.result.data[0];

            // console.log('get new employee: ', result_employee);

                        expect(result_employee).to.have.property("firstname");
            expect(result_employee).to.have.property("lastname");
            expect(result_employee).to.have.property("farm_id");
            expect(result_employee).to.have.property("description");


            let t_id = utils.getIdByDB(db_type, result_employee);
            mockdata2.id = t_id;
            done();
        });
    });

    it('update employee', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'employee', cmd: 'update', data: mockdata2}, function(ignore, result){
            // console.log('update employee: ', result);
            expect(result.result.message).to.equal('Update employee!');
            done();
        });
    });

    it('update not found employee', function(done){
        let seneca = test_seneca(done);
        let mock_not_found = {
            id: 99,
            firstname: "firstname_not_found",
            lastname: "lastname_not_found",
            farm_id: "farm_id_not_found",
            description: "description_not_found"
        };
        seneca.act({ms: 'employee', cmd: 'update', data: mock_not_found}, function(ignore, result){
            let result_employee = result.result;

            expect(result_employee.success).to.equal(false);
            expect(result_employee.data).to.equal(null);

            done();
        });
    });

    it('get updated employee', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'employee', cmd: 'get', data: {id: mockdata2.id}}, function(ignore, result){
            // console.log('get update employee: ', result);
            let result_employee = result.result.data;

                        expect(result_employee).to.have.property("firstname");
            expect(result_employee).to.have.property("lastname");
            expect(result_employee).to.have.property("farm_id");
            expect(result_employee).to.have.property("description");


            done();
        });
    });

    it('create new employee again', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'employee', cmd: 'add', data: mockdata3}, function(ignore, result){
            expect(result.result.message).to.equal('New employee created!');
            done();
        });
    });

    it('get all employee', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'employee', cmd: 'getAll'}, function(ignore, result){
            // console.log('get all employee: ', result.result);

            let results = result.result.data;
            expect(results.length).to.equal(2);
            
            for(let i=0; i<results.length; i++){
                            expect(results[i]).to.have.property("firstname");
            expect(results[i]).to.have.property("lastname");
            expect(results[i]).to.have.property("farm_id");
            expect(results[i]).to.have.property("description");

            }

            done();
        });
    });

    it('delete employee', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'employee', cmd: 'delete', data: {id: mockdata2.id}}, function(ignore, result){
            // console.log('delete employee: ', result);
            let result_employee = result.result;

            expect(result_employee.success).to.equal(true);
            expect(result_employee.message).to.equal('Employee deleted');

            done();
        });
    });

    it('get all employee to make sure deleted employee', function(done){
        let seneca = test_seneca(done);
        seneca.act({ms: 'employee', cmd: 'getAll'}, function(ignore, result){
            let results = result.result.data;
            expect(results.length).to.equal(1);

            done();
        });
    });
});