const config = require('config');
const seneca = require('seneca')({log: 'silent'}).client({port: config.get('user_port'), host: config.get('user_host')});

module.exports = {
    login: function(args) {
        return new Promise((resolve) => {             
            seneca.act({ms: 'user', cmd: 'login', data: args}, function(err, res){
                if (err) console.log('login:', err);
                else resolve(res.result);
            });
        }); 
    }
}