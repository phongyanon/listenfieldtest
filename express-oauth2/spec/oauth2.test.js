let assert = require('assert');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();
chai.use(chaiHttp);

const config = require('config');
let db_type = config.get('db_type');
const utils = require('./helpers/utils');

let oauthModel = require('../model/' + config.get('db_type'));

/**
 * === password grant ===
 * headers
 * 	Authorization: "Basic " + clientId:secret base64'd
 *  Content-Type: application/x-www-form-urlencoded
 * 
 * body
 * 	grant_type=password&username=pedroetb&password=password
 *
 * 
 * === refresh_token grant ===
 * headers
 * 	Authorization: "Basic " + clientId:secret base64'd
 *  Content-Type: application/x-www-form-urlencoded
 * 
 * body
 * 	grant_type=refresh_token&refresh_token=your_token
 * 
 * 
 * === client_credential grant ===
 * headers
 * 	Authorization: "Basic " + clientId:secret base64'd
 * 	Content-Type: application/x-www-form-urlencoded
 * 
 * body
 * 	grant_type=client_credentials
 * 
 * 
 * === using token ===
 * 	Authorization: "Bearer " + access_token
 */

let mockdata = {
	clients: {
		id: 'application',	// TODO: Needed by refresh_token grant, because there is a bug at line 103 in https://github.com/oauthjs/node-oauth2-server/blob/v3.0.1/lib/grant-types/refresh-token-grant-type.js (used client.id instead of client.clientId)
		clientId: 'application',
		clientSecret: 'secret',
		grants: [
			'password',
			'refresh_token'
		],
		redirectUris: []
	},
	confidentialClients: {
		id: 'confidentialApplication',
		clientId: 'confidentialApplication',
		clientSecret: 'topSecret',
		grants: [
			'password',
			'refresh_token',
			'client_credentials'
		],
		redirectUris: []
	},
	tokens: [],
	users: {
		username: 'pedroetb',
		password: 'password'
	}
};

describe('Oauth2 express', function(){
    before(function(done){
		oauthModel.truncateData().then((res) => {
			console.log('clear all data in DB ');
			done();
		}).catch(err => console.log(err));		
	});

    it('Add mockdata into Database', function(done){
			oauthModel.loadExampleData(mockdata).then((res) => {
				console.log('Add mockdata ', res);
				done();
			}).catch(err => console.log(err));
    });

    it('Authenticate with wrong username, password', function(done){
		chai.request(server)
			.post('/auth/token')
			.set('Authorization', 'Basic YXBwbGljYXRpb246c2VjcmV0')
			.set('Contetnt-Type', 'application/x-www-form-urlencoded')
			.type('form')
			.send('grant_type=password')
			.send('username=pedroetb')
			.send('password=password1')
			.end((err, res) => {
				res.should.have.status(400);
				expect(res.body).to.have.property('name').to.equal('invalid_grant');
				done();
			});
    });

    it('Authenticate with correct username, password (get access and refresh token', function(done){
		chai.request(server)
			.post('/auth/token')
			.set('Authorization', 'Basic YXBwbGljYXRpb246c2VjcmV0')
			.set('Contetnt-Type', 'application/x-www-form-urlencoded')
			.type('form')
			.send('grant_type=password')
			.send('username=pedroetb')
			.send('password=password')
			.end((err, res) => {
				res.should.have.status(200);
				expect(res.body).to.have.property('accessToken');
				expect(res.body).to.have.property('accessTokenExpiresAt');
				expect(res.body).to.have.property('refreshToken');
				expect(res.body).to.have.property('refreshTokenExpiresAt');
				expect(res.body).to.have.property('client');
				expect(res.body.client).to.have.property('id');
				expect(res.body).to.have.property('user');
				expect(res.body.user).to.have.property('id');

				mockdata.accessToken = res.body.accessToken;
				mockdata.refreshToken = res.body.refreshToken;
				done();
			});
    });
    
    it('Send access token to get secret data', function(done){
		chai.request(server)
		.get('/')
		.set('Authorization', `Bearer ${mockdata.accessToken}`)
		.end((err, res) => {
			res.should.have.status(200);
			expect(res.body.msg).to.equal('Congratulations, you are in a secret area!');
			done();
		});
    });

		it('send access token to verified before use microservice.', function(done){
			chai.request(server)
			.get('/verifyToken')
			.set('Authorization', `Bearer ${mockdata.accessToken}`)
			.end((err, res) => {
				res.should.have.status(200);
				expect(res.body.status).to.equal('success');
				done();
			});
		});

		it('send false access token to verified before use microservice.', function(done){
			chai.request(server)
			.get('/verifyToken')
			.set('Authorization', `Bearer ${mockdata.accessToken}false`)
			.end((err, res) => {
				res.should.have.status(401);
				expect(res.body.status).to.equal(401);
				expect(res.body.name).to.equal('invalid_token');
				done();
			});
		});

    it('Send refresh token to get new refresh and access token', function(done){
		chai.request(server)
			.post('/auth/token')
			.set('Authorization', 'Basic YXBwbGljYXRpb246c2VjcmV0')
			.set('Contetnt-Type', 'application/x-www-form-urlencoded')
			.type('form')
			.send('grant_type=refresh_token')
			.send(`refresh_token=${mockdata.refreshToken}`)
			.end((err, res) => {
				res.should.have.status(200);
				expect(res.body).to.have.property('accessToken');
				expect(res.body).to.have.property('accessTokenExpiresAt');
				expect(res.body).to.have.property('refreshToken');
				expect(res.body).to.have.property('refreshTokenExpiresAt');
				expect(res.body).to.have.property('client');
				expect(res.body.client).to.have.property('id');
				expect(res.body).to.have.property('user');
				expect(res.body.user).to.have.property('id');

				mockdata.accessToken = res.body.accessToken;
				mockdata.refreshToken = res.body.refreshToken;
				done();
			});
    });

    it('Sign out or revoke token', function(done){
		chai.request(server)
			.post('/signout')
			.send({refresh_token: mockdata.refreshToken})
			.end((err, res) => {
				res.should.have.status(200);
				expect(res.body).to.have.property('msg').to.equal('Successfully Sign Out!');
				done();
			});
	});
	
  it('make sure can not data after sign out', function(done){
		chai.request(server)
		.get('/')
		.set('Authorization', `Bearer ${mockdata.accessToken}`)
		.end((err, res) => {
			res.should.have.status(401);
			expect(res.body).to.have.property('name').to.equal('invalid_token');
			done();
		});
	});
});