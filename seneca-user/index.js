const config = require('config');
const seneca = require('seneca')({ log: 'silent' });
const port = config.get('service_port');
const services = require('./src/services');

seneca.use(services) // , {datastore: config.get('db_type')})
    .listen(port);

console.log(`Listening at ${port}`);
