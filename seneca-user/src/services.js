const request = require('request');
const config = require('config');

const User = require('./user/user');
let user = new User();

module.exports = function(options = {}) {  
    this.add('ms:user,cmd:register', async (msg, respond) => {
        try {
            let result = await user.register(msg.data);
            respond(null, {result: result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:user,cmd:login', async (msg, respond) => {
        try {
            let result = await user.login(msg.data);
            respond(null, {result: result});
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:user,login:true,cmd:get', async (msg, respond) => {
        try {
            let result = await user.getUser(msg.data);
            respond(null, {result: result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:user,login:true,cmd:getByUsername', async (msg, respond) => {
        try {
            let result = await user.getUserByUsername(msg.data);
            respond(null, {result: result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:user,login:true,cmd:add', async (msg, respond) => {
        try {
            let result = await user.addUser(msg.data);
            respond(null, {result: result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:user,login:true,cmd:delete', async (msg, respond) => {
        try {
            let result = await user.deleteUser(msg.data);
            respond(null, {result: result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:user,login:true,cmd:addRole', async (msg, respond) => {
        try {
            let result = await user.addRole(msg.data);
            respond(null, {result: result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:user,login:true,cmd:getAllRole', async (msg, respond) => {
        try {
            let result = await user.getAllRole();
            respond(null, {result: result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:user,login:true,cmd:getRole', async (msg, respond) => {
        try {
            let result = await user.getRole(msg.data);
            respond(null, {result: result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:user,login:true,cmd:updateRole', async (msg, respond) => {
        try {
            let result = await user.updateRole(msg.data);
            respond(null, {result: result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.add('ms:user,login:true,cmd:deleteRole', async (msg, respond) => {
        try {
            let result = await user.deleteRole(msg.data);
            respond(null, {result: result});            
        } catch (err) {
            respond(400, {error: err});
        }
    });

    this.wrap('ms:user,login:true', function(msg, respond){ // this should be the last function
        // check token expired time and verify token
        let that = this;
        if (config.get('env_name') === 'production'){
            let options = {
                url: config.get('authen_url'),
                headers: {
                    'Authorization': `Bearer ${msg.accessToken}`
                }
            }
            request(options, function(err, result){
                if(err) respond(401, {error: err});
                result = JSON.parse(result.body);
                if (result.status === 'success') that.prior(msg, respond);
                else respond(401, result.body);
            });
        } else {
            that.prior(msg, respond);
        }
    });
}
