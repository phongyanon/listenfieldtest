const config = require('config');

module.exports = class Authen {
    constructor() {
        let Query;
        switch(config.get('db_type')){
            case('mongo'):
                Query = require('./database/mongo');
                break;
        }
        
        this.query = new Query();
    }
    register(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let check_username = await query.checkUsername({'username': ctx.username});
                if (check_username === true) {
                    let result = await query.addUser(ctx);
                    resolve(result);
                } else {
                    resolve(false);  // 'This username have been used already.'
                }      
            } catch (err) {
                reject(err.toString());
            }
        });
    }
    login(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                // get username then check password
                let user_result = await query.getUserByUsername(ctx);
                if (user_result.length === 0) resolve({status: 'failed', data: 'invalid username or password'});
                let user = user_result[0];

                let result = await query.checkPassword({'your_password':ctx.password, 'password': user.password});
                if (result === true){
                    resolve({status: 'success', data: user});
                } else {
                    resolve({status: 'failed', data: 'invalid username or password'});
                }
            } catch (err) {
                reject('login: ' + err.toString());
            }
        });
    }    
    getUser(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let user_result = await query.getUser(ctx);
                if (user_result.length > 0){
                    resolve(user_result[0]);
                } else {
                    resolve(false);
                }
            } catch (err) {
                reject(err.toString());
            }
        });
    }
    getUserByUsername(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let user_result = await query.getUserByUsername(ctx);
                if (user_result.length > 0){
                    resolve(user_result[0]);
                } else {
                    resolve(false);
                }
            } catch (err) {
                reject(err.toString());
            }
        });
    }
    addUser(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.addUser(ctx);
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });
    }
    deleteUser(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.deleteUser(ctx);
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });        
    }

    getAllRole(){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.getAllRole();
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    getRole(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.getRole(ctx);
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    addRole(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.addRole(ctx);
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    updateRole(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.updateRole(ctx);
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    deleteRole(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.deleteRole(ctx);
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    resetRole(){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.resetRole();
                resolve('success');
            } catch (err) {
                reject(err.toString());
            }
        });
    }
}