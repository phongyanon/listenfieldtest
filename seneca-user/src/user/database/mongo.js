const config = require('config');
const mongoose = require('mongoose');
const User = require('./mongoModel').userModel;
const Role = require('./mongoModel').roleModel;
const bcrypt = require('bcryptjs');

module.exports = class Query{
    constructor(){
        mongoose.connect(config.get('mongo_host'));
        this.db = mongoose.connection;
        this.saltRounds = 10;
    }

    addUser(ctx){
        return new Promise( async (resolve, reject) => {
            let salt = bcrypt.genSaltSync(this.saltRounds);
            let hash = bcrypt.hashSync(ctx.password, salt);
            ctx.password = hash;

            let user = new User();
            user.username = ctx.username;
            user.password = ctx.password;
            user.user_type_id = ctx.user_type_id;

            user.save(function(err){
                if(err) reject(err.toString());
                resolve({
                    message: 'New User created!',
                    data: user
                })
            });
        });
    }

    getUser(ctx){
        return new Promise( async (resolve, reject) => {
            User.findById(ctx.id, function(err, result){
                if(err) reject(err.toString());
                resolve([result]);
            });
        });
    }

    getUserByUsername(ctx){
        return new Promise( async (resolve, reject) => {
            User.findOne({username: ctx.username}, function(err, result){
                if(err) reject(err.toString());
                if (result === null) resolve([]);
                resolve([result]);
            });
        });
    }

    checkUsername(ctx){
        return new Promise( (resolve, reject) => {
            User.findOne({username: ctx.username}, function(err, result){
                if(err) reject(err.toString());
                if (result !== null){
                    resolve(false);
                } else {
                    resolve(true);
                }
            });
        });
    }

    changePassword(ctx){
        return new Promise( async (resolve, reject) => {
            let salt = bcrypt.genSaltSync(this.saltRounds);
            let hash = bcrypt.hashSync(ctx.password, salt); // encode password
            ctx.password = hash;

            User.findById(ctx.id, function(err, user){
                if(err) reject(err.toString());
                user.password = ctx.password;

                user.save(function(err){
                    if(err) reject(err.toString());
                    resolve({
                        status: "success",
                        message: "Password Changed!",
                    })
                });
            });
        });
    }

    deleteUser(ctx){
        return new Promise( async (resolve, reject) => {
            User.deleteOne({_id: ctx.id}, function(err, result){
                if(err) reject(err.toString());
                resolve({
                    status: "success",
                    message: "User deleted!"
                });
            });
        });
    }

    checkPassword(ctx){
        return new Promise( async (resolve, reject) => {
            let result = bcrypt.compareSync(ctx.your_password, ctx.password);
            resolve(result);
        });
    }

    getAllRole(){
        return new Promise((resolve, reject) => {
            Role.get(function(err, result){
                if(err) reject(err.toString());
                resolve(result);
            });
        });
    }

    getRole(ctx){
        return new Promise((resolve, reject) => {
            Role.findById(ctx.id, function(err, result){
                if(err) reject(err.toString());
                resolve(result);
            });
        });
    }

    addRole(ctx){
        return new Promise((resolve, reject) => {
            let role = new Role();
            role.name = ctx.name;
            role.permission = ctx.permission;

            role.save(function(err){
                if(err) reject(err.toString());
                resolve({
                    message: 'New role created!',
                    data: role
                });
            });
        });
    }

    updateRole(ctx){
        return new Promise((resolve, reject) => {
            Role.findById(ctx.id, function(err, role){
                if(err) reject(err.toString());
                if(role === undefined){
                    resolve({message: 'odject not found!'});
                } else {
                    let props = ['name', 'permission'];
                    for (let i in props){
                        if(ctx.hasOwnProperty(props[i])) role[props[i]] = ctx[props[i]];
                    }
    
                    role.save(function(err){
                        if (err) reject(err.toString());
                        resolve({
                            message: 'Update role!',
                            data: role
                        })
                    });
                }
            });            
        });
    }

    deleteRole(ctx){
        return new Promise((resolve, reject) => {
            Role.deleteOne({_id: ctx.id}, function(err, result){
                if(err) reject(err.toString());
                resolve({
                    status: "success",
                    message: "Role deleted"
                });
            });
        });
    }

    resetRole(){
        return new Promise((resolve, reject) => {
            Role.deleteMany({}, function (err){
                if(err) reject(err.toString());
                resolve(true);
            });
        });
    }
    
}