let mongoose = require('mongoose');

let UserSchema = mongoose.Schema({
    username: {
        type: String,
        require: true
    },
    password: {
        type: String,
        require: true
    },
    user_type_id: Number
})

let RoleSchema = mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    permission: {
        type: String,
        require: true
    },
})

let userModel = mongoose.model('user', UserSchema);
let roleModel = mongoose.model('role', UserSchema);

module.exports = {
    roleModel,
    userModel
}

// let user = module.exports = mongoose.model('user', UserSchema);
// let role = module.exports = mongoose.model('role', RoleSchema);

// module.exports.get = function(callback, limit){
//     user.find(callback).limit(limit);
//     role.find(callback).limit(limit);
// }