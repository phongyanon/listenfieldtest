const config = require('config');
const seneca = require('seneca')({log: 'silent'}).client({port: config.get('lf_port'), host: config.get('lf_host')});

exports.resolver = {
    Query: {
        getAllPlant: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'plant', cmd: 'getAll', accessToken: accessToken}, function(err, res){
                    if (err) console.log('getAllPlant: ', err);
                    else {
                        resolve(res.result);
                    };
                });
            });
        },
        getPlant: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'plant', cmd: 'get', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('getPlant:', err);
                    else resolve(res.result);
                });
            });
        }
    },
    Mutation: {
        addPlant: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'plant', cmd: 'add', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('addPlant: ', err);
                    else resolve(res.result);
                });
            });
        },
        updatePlant: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'plant', cmd: 'update', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('updtePlant: ', err);
                    else resolve(res.result);
                });
            });
        },
        deletePlant: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'plant', cmd: 'delete', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('deletePlant: ', err);
                    else resolve(res.result);
                });
            });
        }
    }
}