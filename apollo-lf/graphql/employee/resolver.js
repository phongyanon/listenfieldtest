const config = require('config');
const seneca = require('seneca')({log: 'silent'}).client({port: config.get('lf_port'), host: config.get('lf_host')});

exports.resolver = {
    Query: {
        getAllEmployee: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'employee', cmd: 'getAll', accessToken: accessToken}, function(err, res){
                    if (err) console.log('getAllEmployee: ', err);
                    else {
                        resolve(res.result);
                    };
                });
            });
        },
        getEmployee: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'employee', cmd: 'get', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('getEmployee:', err);
                    else resolve(res.result);
                });
            });
        }
    },
    Mutation: {
        addEmployee: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'employee', cmd: 'add', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('addEmployee: ', err);
                    else resolve(res.result);
                });
            });
        },
        updateEmployee: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'employee', cmd: 'update', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('updteEmployee: ', err);
                    else resolve(res.result);
                });
            });
        },
        deleteEmployee: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'employee', cmd: 'delete', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('deleteEmployee: ', err);
                    else resolve(res.result);
                });
            });
        }
    }
}