const config = require('config');
const seneca = require('seneca')({log: 'silent'}).client({port: config.get('lf_port'), host: config.get('lf_host')});

exports.resolver = {
    Query: {
        getAllActivity: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'activity', cmd: 'getAll', accessToken: accessToken}, function(err, res){
                    if (err) console.log('getAllActivity: ', err);
                    else {
                        resolve(res.result);
                    };
                });
            });
        },
        getActivity: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'activity', cmd: 'get', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('getActivity:', err);
                    else resolve(res.result);
                });
            });
        }
    },
    Mutation: {
        addActivity: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'activity', cmd: 'add', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('addActivity: ', err);
                    else resolve(res.result);
                });
            });
        },
        updateActivity: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'activity', cmd: 'update', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('updteActivity: ', err);
                    else resolve(res.result);
                });
            });
        },
        deleteActivity: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'activity', cmd: 'delete', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('deleteActivity: ', err);
                    else resolve(res.result);
                });
            });
        }
    }
}