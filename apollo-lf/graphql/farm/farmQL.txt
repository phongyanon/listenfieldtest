query getAllFarm{
  getAllFarm {
    id
    name
    owner_id
    employee_ids
  }
}

query getFarm{
  getFarm(id: "Farm/341378"){
    id
    name
    owner_id
    employee_ids
  }
}

mutation addFarm{
  addFarm(name: "orn", owner_id: "so cute", employee_ids: [1, 2, 3]){
    status
    data
  }
}

mutation updateFarm{
  updateFarm(id: "Farm/341378", name: "orn"){
    status
    data
  }
}

mutation deleteFarm{
  deleteFarm(id: "Farm/341467"){
    status
    data
  }
}