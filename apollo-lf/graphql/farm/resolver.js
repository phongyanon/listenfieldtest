const config = require('config');
const seneca = require('seneca')({log: 'silent'}).client({port: config.get('lf_port'), host: config.get('lf_host')});

exports.resolver = {
    Query: {
        getAllFarm: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'farm', cmd: 'getAll', accessToken: accessToken}, function(err, res){
                    if (err) console.log('getAllFarm: ', err);
                    else {
                        resolve(res.result);
                    };
                });
            });
        },
        getFarm: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'farm', cmd: 'get', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('getFarm:', err);
                    else resolve(res.result);
                });
            });
        }
    },
    Mutation: {
        addFarm: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'farm', cmd: 'add', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('addFarm: ', err);
                    else resolve({
                        status: 'success',
                        data: res.result.message
                    });
                });
            });
        },
        updateFarm: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'farm', cmd: 'update', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('updteFarm: ', err);
                    else resolve({
                        status: 'success',
                        data: res.result.message
                    });
                });
            });
        },
        deleteFarm: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'farm', cmd: 'delete', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('deleteFarm: ', err);
                    else resolve({
                        status: res.result.status,
                        data: res.result.message
                    });
                });
            });
        }
    }
}