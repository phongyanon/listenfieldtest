const config = require('config');
const seneca = require('seneca')({log: 'silent'}).client({port: config.get('lf_port'), host: config.get('lf_host')});

exports.resolver = {
    Query: {
        getAllProcess: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'process', cmd: 'getAll', accessToken: accessToken}, function(err, res){
                    if (err) console.log('getAllProcess: ', err);
                    else {
                        resolve(res.result);
                    };
                });
            });
        },
        getProcess: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'process', cmd: 'get', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('getProcess:', err);
                    else resolve(res.result);
                });
            });
        }
    },
    Mutation: {
        addProcess: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'process', cmd: 'add', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('addProcess: ', err);
                    else resolve(res.result);
                });
            });
        },
        updateProcess: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'process', cmd: 'update', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('updteProcess: ', err);
                    else resolve(res.result);
                });
            });
        },
        deleteProcess: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'process', cmd: 'delete', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('deleteProcess: ', err);
                    else resolve(res.result);
                });
            });
        }
    }
}