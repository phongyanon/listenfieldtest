const config = require('config');
const seneca = require('seneca')({log: 'silent'}).client({port: config.get('lf_port'), host: config.get('lf_host')});

exports.resolver = {
    Query: {
        getAllEquipment: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'equipment', cmd: 'getAll', accessToken: accessToken}, function(err, res){
                    if (err) console.log('getAllEquipment: ', err);
                    else {
                        resolve(res.result);
                    };
                });
            });
        },
        getEquipment: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'equipment', cmd: 'get', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('getEquipment:', err);
                    else resolve(res.result);
                });
            });
        }
    },
    Mutation: {
        addEquipment: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'equipment', cmd: 'add', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('addEquipment: ', err);
                    else resolve(res.result);
                });
            });
        },
        updateEquipment: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'equipment', cmd: 'update', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('updteEquipment: ', err);
                    else resolve(res.result);
                });
            });
        },
        deleteEquipment: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'equipment', cmd: 'delete', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('deleteEquipment: ', err);
                    else resolve(res.result);
                });
            });
        }
    }
}