const config = require('config');
const seneca = require('seneca')({log: 'silent'}).client({port: config.get('lf_port'), host: config.get('lf_host')});

exports.resolver = {
    Query: {
        getAllPlantPlot: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'plantplot', cmd: 'getAll', accessToken: accessToken}, function(err, res){
                    if (err) console.log('getAllPlantPlot: ', err);
                    else {
                        resolve(res.result);
                    };
                });
            });
        },
        getPlantPlot: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'plantplot', cmd: 'get', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('getPlantPlot:', err);
                    else resolve(res.result);
                });
            });
        }
    },
    Mutation: {
        addPlantPlot: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'plantplot', cmd: 'add', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('addPlantPlot: ', err);
                    else resolve(res.result);
                });
            });
        },
        updatePlantPlot: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'plantplot', cmd: 'update', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('updtePlantPlot: ', err);
                    else resolve(res.result);
                });
            });
        },
        deletePlantPlot: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'plantplot', cmd: 'delete', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('deletePlantPlot: ', err);
                    else resolve(res.result);
                });
            });
        }
    }
}