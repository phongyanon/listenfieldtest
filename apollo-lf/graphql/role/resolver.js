const config = require('config');
const seneca = require('seneca')({log: 'silent'}).client({port: config.get('user_port'), host: config.get('user_host')});

exports.resolver = {
    Query: {
        getAllRole: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'user', cmd: 'getAllRole', accessToken: accessToken}, function(err, res){
                    if (err) console.log('getAllRole: ', err);
                    else {
                        resolve(res.result);
                    };
                });
            });
        },
        getRole: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'user', cmd: 'getRole', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('getRole:', err);
                    else resolve(res.result);
                });
            });
        }
    },
    Mutation: {
        addRole: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'user', cmd: 'addRole', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('addRole: ', err);
                    else resolve(res.result);
                });
            });
        },
        updateRole: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'user', cmd: 'updateRole', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('updteRole: ', err);
                    else resolve(res.result);
                });
            });
        },
        deleteRole: (root, args, context, info) => {
            let accessToken = context.token.split(' ')[1];
            return new Promise((resolve) => {
                seneca.act({ms: 'user', cmd: 'deleteRole', data: args, accessToken: accessToken}, function(err, res){
                    if (err) console.log('deleteRole: ', err);
                    else resolve(res.result);
                });
            });
        }
    }
}